package com.diven.common.tree;

import java.io.File;
import java.io.FileInputStream;
import java.util.List;

import com.diven.common.tree.common.Constant;
import com.diven.common.tree.model.ClassifierTree;
import com.diven.common.tree.model.ConvertTypeModel;
import com.diven.common.tree.model.FeatureSpeculate;
import com.diven.common.tree.model.PretreatmentModel;

import junit.framework.TestCase;
import weka.core.Instances;
import weka.core.converters.CSVLoader;
import weka.core.converters.Loader;

public class InteractiveDecisionTreeTest extends TestCase{

	//定义决策树
	private DecisionTree<ClassifierTree> decisionTree = new InteractiveDecisionTree();
	
	
	public void testCsvFile() throws Exception {
		//加载数据
		Loader loader = new CSVLoader();
		loader.setSource(new FileInputStream(new File("src/test/datas/test-01.csv")));
		Instances instances = loader.getDataSet();
		instances.setClassIndex(instances.numAttributes() - 1);
		
		//定义数据预处理模型
		PretreatmentModel pretreatmentModel = new PretreatmentModel();
		ConvertTypeModel convertTypeModel = new ConvertTypeModel();
		convertTypeModel.setFeatureName(instances.classAttribute().name());
		convertTypeModel.setFeatureType(Constant.NOMINAL);
		pretreatmentModel.getConvertTypeModels().add(convertTypeModel);
		instances = decisionTree.pretreatment(instances, pretreatmentModel);
		
		//初始化树
		decisionTree.buildClassifier(instances, 0, 2);
		System.out.println(decisionTree.toDotGraph());
		
		//获取特征推荐
		List<FeatureSpeculate> speculates = decisionTree.getSpeculateListByNodeId(1);
		for(FeatureSpeculate speculate : speculates) {
			System.out.println(speculate);
		}
		
		//使用第一个推荐特征进行分裂
		decisionTree.manualGrowthByNodeId(1, speculates.get(0).getFeatureName());
		System.out.println(decisionTree.toDotGraph());
		
	}
	
	
}
