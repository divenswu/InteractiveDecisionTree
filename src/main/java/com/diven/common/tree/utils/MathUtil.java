package com.diven.common.tree.utils;

import weka.core.ContingencyTables;

public class MathUtil {
	
	/**
	 * 计算方差
	 * @param dist
	 * @return
	 */
	public static double priorVal(double[][] dist) {
        return ContingencyTables.entropyOverColumns(dist);
    }
	
	/**
	 * 计算信息增益
	 * @param dist
	 * @param priorVal
	 * @return
	 */
	public static double gain(double[][] dist, double priorVal) {
        return priorVal - ContingencyTables.entropyConditionedOnRows(dist);
    }
	
}
