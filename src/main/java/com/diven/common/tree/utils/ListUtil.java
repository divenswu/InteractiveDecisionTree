package com.diven.common.tree.utils;

import java.util.List;

public class ListUtil {
	
	/**
	 * join list
	 * @param list
	 * @param separator
	 * @return
	 */
	public static String join(List<?> list, String separator) {
		if(list == null || list.isEmpty()) {
			return "";
		}
		else {
			StringBuffer joinStr = new StringBuffer();
			for(int i=0; i<list.size()-1; i++) {
				joinStr.append(list.get(i)).append(separator);
			}
			joinStr.append(list.get(list.size() - 1));
			return joinStr.toString();
		}
	}
}
