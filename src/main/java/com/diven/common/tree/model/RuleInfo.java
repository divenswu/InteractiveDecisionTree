package com.diven.common.tree.model;

import java.io.Serializable;
import java.util.List;

public class RuleInfo implements Serializable {

	private static final long serialVersionUID = 6300927540098142620L;

	private String nodeNumber;
	private Integer sampleCount;
	private Double hitRatio;
	private Double overRate;
	private Double lift;
	private List<String> ruleList;

	public String getNodeNumber() {
		return nodeNumber;
	}
	
	public void setNodeNumber(String nodeNumber) {
		this.nodeNumber = nodeNumber;
	}
	
	public Integer getSampleCount() {
		return sampleCount;
	}
	
	public void setSampleCount(Integer sampleCount) {
		this.sampleCount = sampleCount;
	}
	
	public Double getHitRatio() {
		return hitRatio;
	}
	
	public void setHitRatio(Double hitRatio) {
		this.hitRatio = hitRatio;
	}
	
	public Double getOverRate() {
		return overRate;
	}
	public void setOverRate(Double overRate) {
		this.overRate = overRate;
	}
	
	public Double getLift() {
		return lift;
	}
	
	public void setLift(Double lift) {
		this.lift = lift;
	}
	
	public List<String> getRuleList() {
		return ruleList;
	}
	
	public void setRuleList(List<String> ruleList) {
		this.ruleList = ruleList;
	}

	@Override
	public String toString() {
		return "RuleInfo [nodeNumber=" + nodeNumber + ", sampleCount="
				+ sampleCount + ", hitRatio=" + hitRatio + ", overRate="
				+ overRate + ", lift=" + lift + ", ruleList=" + ruleList + "]";
	}

}
