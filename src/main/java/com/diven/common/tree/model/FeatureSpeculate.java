package com.diven.common.tree.model;

import java.io.Serializable;

public class FeatureSpeculate implements Serializable{

	private static final long serialVersionUID = -8193340706071652937L;

	private String featureName;
	private String featureType;
	private Double gain;
	private BinPoints binPoints;
	
	public String getFeatureName() {
		return featureName;
	}
	public void setFeatureName(String featureName) {
		this.featureName = featureName;
	}
	public String getFeatureType() {
		return featureType;
	}
	public void setFeatureType(String featureType) {
		this.featureType = featureType;
	}
	public Double getGain() {
		return gain;
	}
	public void setGain(Double gain) {
		this.gain = gain;
	}
	public BinPoints getBinPoints() {
		return binPoints;
	}
	public void setBinPoints(BinPoints binPoints) {
		this.binPoints = binPoints;
	}
	
	@Override
	public String toString() {
		return "FeatureSpeculate [featureName=" + featureName + ", featureType="
				+ featureType + ", gain=" + gain + ", binPoints=" + binPoints
				+ "]";
	}
	
}
