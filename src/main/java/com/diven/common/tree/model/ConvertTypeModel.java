package com.diven.common.tree.model;

import java.io.Serializable;

/**
 * 类型转换模型
 * @author diven
 *
 */
public class ConvertTypeModel implements Serializable{

	private static final long serialVersionUID = 375480337150585934L;

	private String featureName;
	
	private String featureType;

	public String getFeatureName() {
		return featureName;
	}

	public void setFeatureName(String featureName) {
		this.featureName = featureName;
	}

	public String getFeatureType() {
		return featureType;
	}

	public void setFeatureType(String featureType) {
		this.featureType = featureType;
	}
	
}
