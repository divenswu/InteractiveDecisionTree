package com.diven.common.tree.model;

import java.io.Serializable;

/**
 * 缺失值处理模型
 * @author diven
 *
 */
public class MissingValueFillModel implements Serializable{
	
	private static final long serialVersionUID = 1338526424377786374L;

	private String featureName;
	
	private String featureType;
	
	private MissingValueFillType fillType;

	public String getFeatureName() {
		return featureName;
	}

	public void setFeatureName(String featureName) {
		this.featureName = featureName;
	}

	public String getFeatureType() {
		return featureType;
	}

	public void setFeatureType(String featureType) {
		this.featureType = featureType;
	}

	public MissingValueFillType getFillType() {
		return fillType;
	}

	public void setFillType(MissingValueFillType fillType) {
		this.fillType = fillType;
	}

	@Override
	public String toString() {
		return "MissingValueFillModel [featureName=" + featureName
				+ ", featureType=" + featureType + ", fillType=" + fillType
				+ "]";
	}
	
}
