package com.diven.common.tree.model;

import java.io.Serializable;

public class FeatureBinsLabel implements Serializable{

	private static final long serialVersionUID = 3109911276959118424L;

	private String featureName;
	
	private String featureType;
	
	private String binsLabel;

	public String getFeatureName() {
		return featureName;
	}

	public void setFeatureName(String featureName) {
		this.featureName = featureName;
	}

	public String getBinsLabel() {
		return binsLabel;
	}

	public void setBinsLabel(String binsLabel) {
		this.binsLabel = binsLabel;
	}

	public String getFeatureType() {
		return featureType;
	}

	public void setFeatureType(String featureType) {
		this.featureType = featureType;
	}

	@Override
	public String toString() {
		return "FeatureBinsLabel [featureName=" + featureName + ", featureType="
				+ featureType + ", binsLabel=" + binsLabel + "]";
	}
	
}
