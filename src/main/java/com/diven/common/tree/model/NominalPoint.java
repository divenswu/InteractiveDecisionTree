package com.diven.common.tree.model;

import java.util.ArrayList;

public class NominalPoint extends BinPoint<ArrayList<String>>{

	private static final long serialVersionUID = 294816479277612438L;

	/**
	 * 添加一个元素
	 * @param item
	 * @return
	 */
	public NominalPoint add(String item) {
		if(this.getPoint() == null) {
			this.setPoint(new ArrayList<String>());
		}
		this.getPoint().add(item);
		return this;
	}
	
}
