package com.diven.common.tree.model;

public enum MissingValueFillType {
	MISSING_VALLUE,		//缺失值	【离散特征，连续特征】
	MEAN_VALUE,			//均值		【连续特征】
	MODE_VALUE			//众数		【离散特征】
}
