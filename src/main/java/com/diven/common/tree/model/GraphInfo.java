package com.diven.common.tree.model;

import java.io.Serializable;
import java.util.List;

public class GraphInfo implements Serializable{
	
	private static final long serialVersionUID = -6547456723914179451L;

	private String graph;
	
	private List<RuleInfo> infos;

	public String getGraph() {
		return graph;
	}

	public void setGraph(String graph) {
		this.graph = graph;
	}

	public List<RuleInfo> getInfos() {
		return infos;
	}

	public void setInfos(List<RuleInfo> infos) {
		this.infos = infos;
	}

	@Override
	public String toString() {
		return "GraphInfo [graph=" + graph + ", infos=" + infos + "]";
	}
	
}
