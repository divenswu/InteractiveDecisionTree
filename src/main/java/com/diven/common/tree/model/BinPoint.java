package com.diven.common.tree.model;

import java.io.Serializable;

public class BinPoint<Point extends Serializable> implements Serializable{

	private static final long serialVersionUID = -2091423024571344325L;

	private Point point;

	public Point getPoint() {
		return point;
	}

	public void setPoint(Point point) {
		this.point = point;
	}

	@Override
	public String toString() {
		return "BinPoint [point=" + point + "]";
	}
	
}
