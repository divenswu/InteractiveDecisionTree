package com.diven.common.tree.model;

import java.io.Serializable;

public class FeatureBinPoints implements Serializable{

	private static final long serialVersionUID = 2233503015989068622L;

	private String featureName;
	
	private String featureType;
	
	private BinPoints binPoints;

	public String getFeatureName() {
		return featureName;
	}

	public void setFeatureName(String featureName) {
		this.featureName = featureName;
	}

	public String getFeatureType() {
		return featureType;
	}

	public void setFeatureType(String featureType) {
		this.featureType = featureType;
	}

	public BinPoints getBinPoints() {
		return binPoints;
	}

	public void setBinPoints(BinPoints binPoints) {
		this.binPoints = binPoints;
	}

	@Override
	public String toString() {
		return "FeatureBinPoints [featureName=" + featureName + ", featureType="
				+ featureType + ", binPoints=" + binPoints + "]";
	}
	
}
