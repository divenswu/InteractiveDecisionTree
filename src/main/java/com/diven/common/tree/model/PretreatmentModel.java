package com.diven.common.tree.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 数据预处理模型
 * @author diven
 */
public class PretreatmentModel implements Serializable{

	private static final long serialVersionUID = -3517837236046888371L;
	
	/**特征过滤*/
	private List<String> filterAttributes = new ArrayList<>();
	/**类型转换模型*/
	private List<ConvertTypeModel> convertTypeModels = new ArrayList<>();
	/**缺失值处理*/
	private List<MissingValueFillModel> missingValueFillModels = new ArrayList<>();

	public List<String> getFilterAttributes() {
		return filterAttributes;
	}

	public void setFilterAttributes(List<String> filterAttributes) {
		this.filterAttributes = filterAttributes;
	}
	public List<ConvertTypeModel> getConvertTypeModels() {
		return convertTypeModels;
	}
	
	public void setConvertTypeModels(List<ConvertTypeModel> convertTypeModels) {
		this.convertTypeModels = convertTypeModels;
	}

	public List<MissingValueFillModel> getMissingValueFillModels() {
		return missingValueFillModels;
	}

	public void setMissingValueFillModels(List<MissingValueFillModel> missingValueFillModels) {
		this.missingValueFillModels = missingValueFillModels;
	}

	@Override
	public String toString() {
		return "PretreatmentModel [filterAttributes=" + filterAttributes
				+ ", convertTypeModels=" + convertTypeModels
				+ ", missingValueFillModels=" + missingValueFillModels + "]";
	}

}
