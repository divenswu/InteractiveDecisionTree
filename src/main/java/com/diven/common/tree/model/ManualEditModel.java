package com.diven.common.tree.model;

import java.io.Serializable;

public class ManualEditModel implements Serializable{
	private static final long serialVersionUID = 8286650222858232656L;

	private Long id;
	
	private Integer nodeId;
	
	private String attrName;
	
	private BinPoints binPoints;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getNodeId() {
		return nodeId;
	}

	public void setNodeId(Integer nodeId) {
		this.nodeId = nodeId;
	}

	public String getAttrName() {
		return attrName;
	}

	public void setAttrName(String attrName) {
		this.attrName = attrName;
	}

	public BinPoints getBinPoints() {
		return binPoints;
	}

	public void setBinPoints(BinPoints binPoints) {
		this.binPoints = binPoints;
	}

	@Override
	public String toString() {
		return "ManualEditModel [id=" + id + ", nodeId=" + nodeId + ", attrName=" + attrName + ", binPoints="
				+ binPoints + "]";
	}
	
}
