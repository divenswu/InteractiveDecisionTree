package com.diven.common.tree;

import java.io.Serializable;
import java.util.List;

import com.diven.common.tree.model.BinPoints;
import com.diven.common.tree.model.FeatureBinPoints;
import com.diven.common.tree.model.FeatureSpeculate;
import com.diven.common.tree.model.PretreatmentModel;
import com.diven.common.tree.model.RuleInfo;

import weka.core.Instances;

public interface DecisionTree<Tree> extends Serializable{
	
	/**
	 * 获取当前的决策树
	 * @return
	 */
	public Tree getClassifierTree();
	
	
	/**
	 * 数据预处理
	 * @param instances
	 * @param pretreatmentModel
	 * @return
	 */
	public Instances pretreatment(Instances instances, PretreatmentModel pretreatmentModel) throws Exception;
	
	/**
	 * 初始化构建树
	 * @param instances	数据集
	 * @throws Exception
	 */
	public void buildClassifier(Instances instances) throws Exception;
	
	/**
	 * 初始化构建树
	 * @param instances	数据集
	 * @param maxDepth	树的最大深度
	 * @param minNum	每个叶子的最小实例数
	 * @throws Exception
	 */
	public void buildClassifier(Instances instances, int maxDepth, double minNum) throws Exception;
	
	/**
	 * 根据ID获取推荐列表
	 * @param nodeNo
	 * @return
	 * @throws Exception
	 */
	public List<FeatureSpeculate> getSpeculateListByNodeId(Integer nodeNo) throws Exception;
	
	/**
	 * 根据ID获取推荐列表
	 * @param nodeNo
	 * @param threadNum
	 * @param maxDepth
	 * @return
	 * @throws Exception
	 */
	public List<FeatureSpeculate> getSpeculateListByNodeId(Integer nodeNo, int threadNum, int maxDepth) throws Exception;
	
	/**
	 * 手动生长树节点
	 * @param nodeNo
	 * @param feature
	 * @return
	 * @throws Exception
	 */
	public boolean manualGrowthByNodeId(Integer nodeNo, String feature) throws Exception;
	
	/**
	 * 手动生长树节点
	 * @param nodeNo
	 * @param feature
	 * @param maxDepth
	 * @return
	 * @throws Exception
	 */
	public boolean manualGrowthByNodeId(Integer nodeNo, String feature, int maxDepth) throws Exception;
	
	/**
	 * 获取当前节点的分箱点
	 * @param nodeNo
	 * @return
	 */
	public FeatureBinPoints getBinPointsByNodeId(Integer nodeNo);
	
	/**
	 * 编辑分箱点
	 * @param nodeNo
	 * @param feature
	 * @param binPoints
	 * @throws Exception
	 */
	public boolean manualEditByNodeId(Integer nodeNo, String feature, BinPoints binPoints) throws Exception;
	
	/**
	 * 根据nodeNo树剪枝
	 * @param nodeNo
	 * @throws Exception
	 */
	public void manualPruneByNodeId(Integer nodeNo) throws Exception;
	
	/**
	 * 获取叶子节点的规则信息
	 * @return
	 */
	public List<RuleInfo> getRuleInfoForleafNodes();
	
	/**
	 * <p>将模型转换为DOT图</p>
	 * <p>图可使用：http://viz-js.com 进行渲染</p>
	 * @return
	 * @throws Exception
	 */
	public String toDotGraph() throws Exception;
	
}
